package com.iteaj.iot.client.websocket;

import com.iteaj.iot.client.codec.WebSocketClient;
import com.iteaj.iot.websocket.WebSocketComponent;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker;

public interface WebSocketClientComponent<M extends WebSocketClientMessage> extends WebSocketComponent<M> {

    WebSocketClientHandshaker createClientHandShaker(WebSocketClient client);
}
