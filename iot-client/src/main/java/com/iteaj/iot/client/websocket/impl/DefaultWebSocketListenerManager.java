package com.iteaj.iot.client.websocket.impl;

import com.iteaj.iot.LifeCycle;
import com.iteaj.iot.client.websocket.WebSocketClientConnectProperties;
import com.iteaj.iot.client.websocket.WebSocketClientListener;
import com.iteaj.iot.websocket.WebSocketException;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultWebSocketListenerManager implements LifeCycle {

    private List<WebSocketClientListener> listeners;
    private List<WebSocketClientConnectProperties> listenerConfig = new ArrayList<>();
    private Map<String, WebSocketClientListener> uriListenerMap = new HashMap<>(16);

    public DefaultWebSocketListenerManager(List<WebSocketClientListener> listeners) {
        this.listeners = listeners;
    }

    public List<WebSocketClientListener> getListeners() {
        return listeners;
    }

    public WebSocketClientListener getListenerByUri(String uri) {
        return uriListenerMap.get(uri);
    }

    public WebSocketClientListener getListenerByURI(URI uri) {
        return uriListenerMap.get(uri.getAuthority() + uri.getPath());
    }

    public List<WebSocketClientConnectProperties> getListenerConfig() {
        return listenerConfig;
    }

    @Override
    public void start(Object config) {
        // 连接所有需要监听的客户端
        this.getListeners().forEach(item -> {
            WebSocketClientConnectProperties properties = item.properties();

            String path = properties.getUri().getPath();
            String authority = properties.getUri().getAuthority();

            WebSocketClientListener listener = uriListenerMap.get(authority + path);
            if(listener == null) {
                properties.bindListener(item);
                listenerConfig.add(properties);
                uriListenerMap.put(authority + path, item);
            }
        });
    }

    @Override
    public void close() {

    }
}
