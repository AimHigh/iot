package com.iteaj.iot.event;

public interface ExceptionEventListener extends FrameworkEventListener<ExceptionEvent> {

    @Override
    default boolean isMatcher(IotEvent event) {
        return event instanceof ExceptionEvent;
    }

    void onEvent(ExceptionEvent event);
}
