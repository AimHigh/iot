package com.iteaj.iot.resolver;

import java.util.function.Function;

public interface FunctionResolver<T, R> extends ResultResolver, Function<T, R> {

}
