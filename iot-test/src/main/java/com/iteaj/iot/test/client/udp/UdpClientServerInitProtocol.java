package com.iteaj.iot.test.client.udp;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.test.TestProtocolType;

public class UdpClientServerInitProtocol extends ServerInitiativeProtocol<UdpClientTestMessage> {

    public UdpClientServerInitProtocol(UdpClientTestMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected void doBuildRequestMessage(UdpClientTestMessage requestMessage) {

    }

    @Override
    protected UdpClientTestMessage doBuildResponseMessage() {
        return null;
    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.PIReq;
    }
}
