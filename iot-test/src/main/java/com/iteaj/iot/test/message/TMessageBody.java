package com.iteaj.iot.test.message;

import com.iteaj.iot.message.DefaultMessageBody;

public class TMessageBody extends DefaultMessageBody {

    public TMessageBody() {
        super();
    }

    public TMessageBody(byte[] message) {
        super(message);
    }
}
