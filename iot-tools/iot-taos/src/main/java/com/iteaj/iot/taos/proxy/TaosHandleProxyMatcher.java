package com.iteaj.iot.taos.proxy;

import com.iteaj.iot.handle.proxy.ProtocolHandleProxy;
import com.iteaj.iot.taos.TaosHandle;
import com.iteaj.iot.taos.TaosSqlManager;
import com.iteaj.iot.tools.db.DBManager;
import com.iteaj.iot.tools.db.DBProtocolHandleProxyMatcher;

public class TaosHandleProxyMatcher implements DBProtocolHandleProxyMatcher {

    private final TaosSqlManager taosSqlManager;

    public TaosHandleProxyMatcher(TaosSqlManager taosSqlManager) {
        this.taosSqlManager = taosSqlManager;
    }

    @Override
    public boolean matcher(Object target) {
        return target instanceof TaosHandle;
    }

    @Override
    public DBManager getDbManager() {
        return taosSqlManager;
    }

    @Override
    public Class<? extends ProtocolHandleProxy> getProxyClass() {
        return TaosHandle.class;
    }
}
